import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App'
import * as firebase from 'firebase'
import router from './router'
import { store } from './store'
import DateFilter from './filters/date'
import AlertCmp from './components/Shared/Alert'

Vue.use(Vuetify)
Vue.config.productionTip = false

Vue.filter('date', DateFilter)
Vue.component('app-alert', AlertCmp)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyAdBUubeSdxCyOdSuePF8sG96bCSZTzlSc',
      authDomain: 'meetups-2daf5.firebaseapp.com',
      databaseURL: 'https://meetups-2daf5.firebaseio.com',
      projectId: 'meetups-2daf5',
      storageBucket: 'gs://meetups-2daf5.appspot.com',
      messagingSenderId: '1003909264359'
    })
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoSignIn', user)
      }
    })
    this.$store.dispatch('loadMeetups')
  }
})
